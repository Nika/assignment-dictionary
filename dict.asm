%include "lib.inc"

global find_word
section .text

find_word:
    
.loop:    
    push rdi
    push rsi
    add rsi, 8
    call string_equals
    pop rsi
    pop rdi
    cmp rax, 1
    je .end
    mov rsi, [rsi]
    cmp rsi, 0
    jne .loop
    mov rax, 0
    ret

.end:
    mov rax, rsi
    ret
