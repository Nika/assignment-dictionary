%ifndef PREV
  %define PREV 0
%endif
%macro colon 2
  %%label: dq PREV
    db %1, 0
  %2:  
  ; redefine PREV
  %define PREV %%label 
%endmacro
