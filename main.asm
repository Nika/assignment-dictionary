%include "lib.inc"
%include "words.inc"

%define STDERR 2
%define STDOUT 1
%define NEWLINE 0xA
%define RESERVED 8
%define BUFFER_SIZE 256 ; Размер выделенной области в стеке
%define BUFFER_CAPACITY 255 ; Максимальное количество считанных символов 

extern find_word

section .rodata
error_key_message: db "No such key in dictionary",0
error_input_message: db "Incorrect input",0

section .text
global _start

_start:
    xor r9, r9
    sub rsp, BUFFER_SIZE ; Выделяем место в стеке
.loop:
    call read_char
    test rax, rax
    jz .check
    cmp rax, NEWLINE
    je .check
    mov [rsp + r9], al
    inc r9
    cmp r9, BUFFER_CAPACITY
    ja .error_input
    jmp .loop

.check:
    test r9, r9
    jz .error_input
    xor rax, rax
    mov [rsp + r9], al ; Дописываем нуль-терминатор в конец строки

.search:
    mov rdi, rsp
    mov rsi, PREV
    call find_word
    add rsp, BUFFER_SIZE ; Освобождаем выделенное место 
    test rax, rax
    jz .error_key
    mov rdi, rax
    add rdi, RESERVED
    call string_length
    add rdi, rax
    inc rdi
    call print_string_stdout
    mov rdi, STDOUT
    call print_newline
    xor rdi, rdi
    jmp .end

.error_input:
    mov rdi, error_input_message
    jmp .err_end
    
.error_key:
    mov rdi, error_key_message

.err_end:
    call print_string_stderr
    mov rdi, STDERR
    call print_newline
    mov rdi, 1

.end:
    jmp exit
