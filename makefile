ASM        = nasm
ASMFLAGS   =-felf64 -g
LD         = ld
RM         = rm -rf
OBJDIR     = obj
BUILDDIR   = build

$(BUILDDIR)/main: $(OBJDIR)/main.o $(OBJDIR)/dict.o $(OBJDIR)/lib.o
	$(LD) -o $@ $^

create_dir:
	mkdir -p $(BUILDDIR)
	mkdir -p $(OBJDIR)

$(OBJDIR)/main.o: main.asm create_dir
	$(ASM) $(ASMFLAGS) -o $@ $<

$(OBJDIR)/dict.o: dict.asm create_dir
	$(ASM) $(ASMFLAGS) -o $@ $<

$(OBJDIR)/lib.o: lib.asm create_dir
	$(ASM) $(ASMFLAGS) -o $@ $<

clean: 
	$(RM) $(OBJDIR) $(BUILDDIR)
